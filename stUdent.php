<!DOCTYPE html>
<html>
<head>
<title>Creating Dynamic Data Graph using PHP and Chart.js</title>
<style type="text/css">
BODY {
    width: 550PX;
}

#chart-container {
    width: 100%;
    height: auto;
}
</style>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>



</head>
<body>
    <div id="chart-container">
        <canvas id="graphCanvas"></canvas>
    </div>

    <script>
        $(document).ready(function () {
            
            showGraph();
        });


        function showGraph()
        {
            {
                $.post("phpfile.php",
                function (data)
                {
                    console.log(data);
                     var name = [];
                    var marks = [];
                    var height=[];

                       var jsonfile =[{"height":"85","st_name":"Name1"},{"height":"100","st_name":"Name3"},{"height":"92","st_name":"Name4"},{"height":"104","st_name":"Name5"},{"height":"91","st_name":"Name2"},{"height":"99","st_name":"Name6"},{"height":"140","st_name":"AI346"},{"height":"139","st_name":"abc"},{"height":"141","st_name":"def"},{"height":"140","st_name":"ghi"},{"height":"144","st_name":"jkl"},{"height":"130","st_name":"lmn"},{"height":"142","st_name":"opq"},{"height":"132","st_name":"rst"},{"height":"135","st_name":"xyz"},{"height":"135","st_name":"asdfsf"}];
                       const arr =new Array(jsonfile.length).fill(null);


                    for (var i in data) {
                        name.push(data[i].st_name);
                        marks.push(data[i].height);
                        //height.push(data[i].Term2_height


                    }
               for (var i = 0; i < arr.length; i++) {
                        //if(jsonfile[i].height==100)
                             height.push(jsonfile[i].height);
                              name.push(jsonfile[i].st_name);
                    }
                    console.log(height);
                    
                    for (var i=0;i<jsonfile.length;i++)
                    {  //console.log("hi");
                        if(jsonfile[i].height==91)
                         {arr[i]=jsonfile[i].height;
                            //console.log("ifhi");
                        }

                      
                    }
                    console.log(arr);

                    var chartdata = {
                        labels: name,
                        datasets: [
                            {
                                label: 'Term1_height',
                                fill:false,
                                lineTension:0.05,
                                backgroundColor: '#5B2C6F',
                                borderColor: '#5B2C6F',
                                pointHoverBackgroundColor: '#5B2C6F',
                                pointHoverBorderColor: '#5B2C6F',
                                data: marks
                                //data:height
                            },

                            {
                                label: 'particular Student ',
                                fill:false,
                                lineTension:0.1,
                                backgroundColor: '#C0392B',
                                borderColor: '#C0392B',
                                pointHoverBackgroundColor: '#C0392B',
                                pointHoverBorderColor: '#C0392B',
                                pointHoverRadius:2,
                                data: arr
                                //data:height
                            },
                            {
                                label: 'height',
                                fill:false,
                                lineTension:0.1,
                                backgroundColor: '#99ff33',
                                borderColor: '#99ff33',
                                pointHoverBackgroundColor: '#C0392B',
                                pointHoverBorderColor: '#C0392B',
                                data: height
                                //data:height
                            }


                            ]
                            
                    };

                    var graphTarget = $("#graphCanvas");

                    var lineGraph = new Chart(graphTarget, {
                        type: 'line',
                        data: chartdata,
                        options:{
                            scales:{
                                xAxes:[{
                                    display: false
                                }]
                            }
                        }
                    });
                });
            }
        }
        </script>

</body>
</html>
